Subject: New topic from {AUTHOR_NAME}

Hi,

"{AUTHOR_NAME}" posted a new topic in "{SITENAME}".

To see the created topic, use this link:
{U_ITEM_LINK}

To stop receiving these types of notifications, use this link:
{U_UNSUBSCRIBE}

{EMAIL_SIG}
