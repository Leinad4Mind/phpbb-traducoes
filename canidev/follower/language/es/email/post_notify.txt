Subject: Nuevo mensaje de {AUTHOR_NAME}

Hola,

"{AUTHOR_NAME}" ha publicado un nuevo mensaje en "{SITENAME}".

Para ver el mensaje, use este enlace:
{U_ITEM_LINK}

Para dejar de recibir este tipo de notificaciones, use este enlace:
{U_UNSUBSCRIBE}

{EMAIL_SIG}
