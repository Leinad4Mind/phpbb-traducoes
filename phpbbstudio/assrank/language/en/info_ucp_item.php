<?php
/**
 *
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'UCP_ASS_USER_RANK'					=> 'User rank',
	'UCP_ASS_USER_RANK_DESC'			=> 'Here you can revert from your special rank back to your regular post count rank.',
	'UCP_ASS_USER_RANK_REVERT'			=> 'Revert user rank',
	'UCP_ASS_USER_RANK_REVERT_CONFIRM'	=> 'Are you sure you wish to delete your user rank?<br>This action can not be undone.',
	'UCP_ASS_USER_RANK_REVERT_SUCCESS'	=> 'You have successfully reverted your user rank.',
	'UCP_ASS_USER_RANK_REVERT_SHORT'	=> 'Revert',
]);
