<?php
/**
 *
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_GROUP_CREATE'			=> 'Create group',
	'ASS_TYPE_GROUP_CREATE_CONFIRM'	=> 'Are you sure you wish to create a group?',
	'ASS_TYPE_GROUP_CREATE_LOG'		=> 'created a group',
	'ASS_TYPE_GROUP_CREATE_SUCCESS'	=> 'You have successfully created a group.',
	'ASS_TYPE_GROUP_CREATE_TITLE'	=> 'Create a group',

	'ASS_TYPE_GROUP_JOIN'			=> 'Join group',
	'ASS_TYPE_GROUP_JOIN_CONFIRM'	=> 'Are you sure you wish to join this group?',
	'ASS_TYPE_GROUP_JOIN_DELETED'	=> 'The group that was selected no longer exists!',
	'ASS_TYPE_GROUP_JOIN_LOG'		=> 'joined a group',
	'ASS_TYPE_GROUP_JOIN_SUCCESS'	=> 'You have successfully joined this group.',
	'ASS_TYPE_GROUP_JOIN_TITLE'		=> 'Join a group',
	'ASS_TYPE_GROUP_JOIN_NONE'		=> '<strong>Groups not yet created!</strong>',

	'ASS_TYPE_GROUP_LEADER'			=> 'Make user group leader',
	'ASS_TYPE_GROUP_LEADER_EXPLAIN'	=> 'This will automatically make the creator of a group the group leader. A group leader can change almost all settings regarding a group.',
]);
