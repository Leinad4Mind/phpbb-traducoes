<?php

/**
 *
 * Invite. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 4.2.0] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2010-2015, KillBill
 * @copyright (c) 2017, kasimi
 * @copyright (c) 2017-2018, Leinad4Mind, https://leinad4mind.top/forum
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
// Some characters you may want to copy&paste: ’ « » „ “ ” …

$lang = array_merge($lang, array(
	'ACC_INVITE_DISABLE'								=> 'Convite desativado',
	'ACC_INVITE_ENABLE'									=> 'Somente com convite',
	'ACC_INVITE_REGISTER'								=> 'Convite + Registo',
	'ACP_CAT_INVITE'									=> 'phpBB Invite',
	'ACP_INVITE_REGISTER'								=> 'Criando ID por convite',
	'ACP_INVITE_REGISTER_EXPLAIN'						=> 'definir opções de registro, pode ser usado como um único recurso, se necessário.<br />Desativar registro substitui todas as opções!',
	'ACP_INVITE'										=> 'Convite',
	'ACP_INVITET_PEND_COUNT'							=> 'Todos Convites (%s)',
	'ACP_INVITET_USERS_COUNT'							=> 'Todos os usuários (%s)',
	'ACP_INVITE_ALL_USER_INVITE_MAX'					=> 'Convite ilimitado para todos',
	'ACP_INVITE_ALL_USER_INVITE_MAX_EXPLAIN'			=> 'se selecionado, todos os usuários terão convites ilimitados.',
	'ACP_INVITE_BIRTHDAYS'								=> 'Aniversário',
	'ACP_INVITE_BIRTHDAYS_EXPLAIN'						=> 'Você pode definir quantos convites um usuário receberá em seu aniversário. <em>Para que essa configuração tenha efeito, o recurso de aniversário também precisa ser ativado.</em>',
	'ACP_INVITE_CAPTCHA'								=> 'Confirmação visual',
	'ACP_INVITE_CAPTCHA_EXPLAIN'						=> 'Os usuários devem inserir um código aleatório de uma imagem.',
	'ACP_INVITE_CONFIG'									=> 'Configurações',
	'ACP_INVITE_CONFIG_EXPLAIN'							=> 'Aqui você poderá configurar a extensão de convite.',
	'ACP_INVITE_CONFIG_GIFT'							=> 'Configurações de presente',
	'ACP_INVITE_CONFIG_GROUP'							=> 'Configurações de grupo',
	'ACP_INVITE_CONFIG_REQUEST'							=> 'Configurações de solicitações',
	'ACP_INVITE_DB'										=> 'peça',
	'ACP_INVITE_DEFAULT_GROUP'							=> 'Tornar o grupo padrão',
	'ACP_INVITE_DEFAULT_GROUP_EXPLAIN'					=> 'O grupo selecionado também será o grupo padrão para usuários convidados.<br /><em>Observe que essa configuração substitui a configuração "Definir usuários recém-registrados como padrão".</em>',
	'ACP_INVITE_DEFAULT_GROUP_USERS'					=> 'Todos os usuários no grupo',
	'ACP_INVITE_DEFAULT_GROUP_USERS_EXPLAIN'			=> 'Você precisa selecionar no mínimo um grupo. Você precisa selecionar sim ou não para especificar se todos os usuários desse grupo selecionado são afetados ou apenas se os usuários são membros do grupo padrão.',
	'ACP_INVITE_DELETES_PEND_CONFIRM'					=> 'Tem certeza de que deseja excluir os convites selecionados?<br /><em>Observação: Se você confirmar isso, as pessoas convidadas não poderão mais usar seu convite!</em>',
	'ACP_INVITE_DELETE_ALL_PEND_CONFIRM'				=> 'Tem certeza de que deseja excluir todos os convites?<br /><em>Observação: Se você confirmar isso, os convidados não poderão mais usar seu convite!</em>',
	'ACP_INVITE_DELETE_GROUPS_CONFIRM'					=> 'Tem certeza de que deseja excluir os convites de grupos selecionados?',
	'ACP_INVITE_DELETE_GROUP_CONFIRM'					=> 'Tem certeza de que deseja excluir os convites do grupo selecionado?',
	'ACP_INVITE_DELETE_PEND_CONFIRM'					=> 'Tem certeza de que deseja excluir o convite selecionado?<br /><em>Observação: Se você confirmar isso, a pessoa convidada não poderá mais usar o convite!</em>',
	'ACP_INVITE_DELETE_TIME'							=> 'Expirar tempo para excluir convites',
	'ACP_INVITE_DELETE_TIME_EXPLAIN'					=> 'Você pode definir a hora em que um usuário pode excluir convites enviados para o endereço de e-mail errado.',
	'ACP_INVITE_DELETE_USERS_CONFIRM'					=> 'Tem certeza de que deseja excluir os convites dos usuários selecionados?',
	'ACP_INVITE_DELETE_USER_CONFIRM'					=> 'Tem certeza de que deseja excluir os convites do usuário selecionado?',
	'ACP_INVITE_DISABLED'								=> 'O sistema de convite está desativado!',
	'ACP_INVITE_DENY'									=> 'Rejeição',
	'ACP_INVITE_EMAIL_BOARD_DISABLED'					=> '<strong>phpBB Invite:</strong> O envio de e-mails está desativado no momento!<br />Enquanto estiver desativado, os convites não devem funcionar. Se você quiser ativa-lo, você pode fazê-lo %sAQUI%s.',
	'ACP_INVITE_EMAIL_DISABLED'							=> 'Envio de e-mails foi desativado, por favor, ative-o. %sVá às configurações de e-mail.%s ',
	'ACP_INVITE_EMAIL_PRIORITY'							=> 'Prioridade do email de convite',
	'ACP_INVITE_ENABLE'									=> 'Ativar extensão de convite',
	'ACP_INVITE_ENABLE_EXPLAIN'							=> 'Se você ativá-lo, a extensão do convite será usada.',
	'ACP_INVITE_VALIDATION'								=> 'Tempo de expiração dos convites enviados',
	'ACP_INVITE_VALIDATION_EXPLAIN'						=> 'Você pode definir o tempo de expiração dos convites enviados. Após esse período, o ID do convite será excluído, portanto, não é mais possível registrá-lo.',
	'ACP_INVITE_EXPLAIN'								=> 'O valor será adicionado.',
	'ACP_INVITE_GIFT_NUMBER_INVITES'					=> 'Número de convites presenteado',
	'ACP_INVITE_GIFT_INVITED_USERS'						=> 'Usuários convidados',
	'ACP_INVITE_GIFT_INVITED_USERS_EXPLAIN'				=> 'Os usuários receberão um convite de presente após esse número de usuários convidados.<br /><em>Se "0", essa função será desativada.</em>',
	'ACP_INVITE_GROUP'									=> 'Grupo de usuários convidados',
	'ACP_INVITE_GROUP_ENABLED'							=> 'Ativar o uso de grupo',
	'ACP_INVITE_GROUP_ENABLED_EXPLAIN'					=> 'If enabled, invited users will automatically be added to the selected group.',
	'ACP_INVITE_GROUP_EXPLAIN'							=> 'The invited users will be added to the selected group.<br /><em>Note that the pre-defined system groups are not on the list.</em>',
	'ACP_INVITE_GROUPS_MANAGEMENT'						=> 'Manage Group',
	'ACP_INVITE_CLAIMING'									=> 'Invitation requests',
	'ACP_INVITE_CLAIMING_EXPLAIN'							=> 'View / decide about users invitation requests.',
	'ACP_INVITE_INDEX_TITLE'							=> 'Welcome to phpBB Invite!',
	'ACP_INVITE_INDEX_TITLE_EXPLAIN'					=> 'Thank you for choosing phpBB Invite. On this page you can see an overview of various statistical data of the invite system. On top of the page and via the links on the left side of the invite you can change every part in detail. On every page you will find a description of the current settings.',
	'ACP_INVITE_INSTALL'								=> 'Installation date',
	'ACP_INVITE_HANDLING'								=> 'Manage users/groups',
	'ACP_INVITE_HANDLING_EXPLAIN'						=> 'Here you can grant or revoke the invitations for users and groups.',
	'ACP_INVITE_HANDLING_GROUPS_DELETE'					=> 'The invitations of the following group were deleted successfully. <br /><br />(<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_GROUPS_DELETES'				=> 'The invitations of the following groups were deleted successfully. <br /><br />(<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_GROUPS_IG'						=> 'Successfully delivered the invitation to (<em><b>%s</b></em>) group.<br />Sent invitation: (<em><b>%s</b></em>)',
	'ACP_INVITE_HANDLING_GROUPS_IGS'					=> 'Successfully delivered the invitation to (<em><b>%s</b></em>) groups.<br />Sent invitation: (<em><b>%s</b></em>)',
	'ACP_INVITE_HANDLING_GROUPS_ISG'					=> 'Successfully delivered the invitations to (<em><b>%s</b></em>) group.<br />Sent invitations: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_GROUPS_ISGS'					=> 'Successfully delivered the invitations to (<em><b>%s</b></em>) groups.<br />Sent invitations: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USERS_DELETE'					=> 'The invitations of the following user were deleted successfully<br /><br />(<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USERS_DELETES'					=> 'The invitations of the following users were deleted successfully<br /><br />(<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USERS_ISU'						=> 'Successfully delivered the invitations to (<em><b>%s</b></em>) user.<br />Sent invitations: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USERS_ISUS'					=> 'Successfully delivered the invitations to (<em><b>%s</b></em>) users.<br />Sent invitations: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USERS_IU'						=> 'Successfully delivered the invitation to (<em><b>%s</b></em>) user.<br /> Sent invitation: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USERS_IUS'						=> 'Successfully delivered the invitation to (<em><b>%s</b></em>) users.<br />Sent invitation: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USER_I'						=> 'Number of invitation changed successfully for user (<em><b>%s</b></em>).<br />New quantity: (<em><b>%s</b></em>).',
	'ACP_INVITE_HANDLING_USER_IS'						=> 'Number of invitations changed successfully for user (<em><b>%s</b></em>).<br />New quantity: (<em><b>%s</b></em>).',
	'ACP_INVITE_ALLOCATE'								=> 'Allocation',
	'ACP_INVITE_SEND_DAY_DB'							=> 'Number of invitations to be sent per day',
	'ACP_INVITE_SEND_DAY_DB_EXPLAIN'					=> 'You can set the maximum number of invites a user can send per day.<br /><em>(0=unlimited.)</em>',
	'ACP_INVITE_SEND_DB_NUM'							=> 'number of pieces',
	'ACP_INVITE_MAIL_FIX'								=> 'Allow e-mail address switching',
	'ACP_INVITE_MAIL_FIX_EXPLAIN'						=> 'It allows to specify a different e-mail address during the registration.',
	'ACP_INVITE_MAIN'									=> 'Overview',
	'ACP_INVITE_MAINTENANCE_UPDATE_INVITE'				=> 'The oldest version the phpBB Invite updater supports is <strong>“%s”</strong>. Please update to version <strong>“%s”</strong> before running the update to version <strong>“%s”</strong>.',
	'ACP_INVITE_MAINTENANCE_UPDATE_PHPBB'				=> 'You need to update phpBB to at least version <strong>“%s”</strong> before updating phpBB Invite to version <strong>“%s”</strong>.',
	'ACP_INVITE_MAIN_CONFIG'							=> 'Main settings',
	'ACP_INVITE_MEMBER'									=> '%s Person',
	'ACP_INVITE_MESSAGE'								=> 'User justification',
	'ACP_INVITE_MESSAGE_TIME'							=> 'Request time',
	'ACP_INVITE_MINUS'									=> 'You can’t set a minus value.',
	'ACP_INVITE_MOD'									=> 'phpBB Invite',
	'ACP_INVITE_MORE_EMAIL_DB'							=> 'Set recipients limit',
	'ACP_INVITE_MORE_EMAIL_DB_EXPLAIN'					=> 'You can set the maximum number of invitations that can be sent at a time here.<br /><em>(1=disabled.)</em>',
	'ACP_INVITE_NEW_MEMBER_POST_LIMIT_IGNORED'			=> 'Ignore new members post limit',
	'ACP_INVITE_NEW_MEMBER_POST_LIMIT_IGNORED_EXPLAIN'	=> 'If set to yes, accepting an invitation will not get newly registered users into the “Newly registered users” group.',
	'ACP_INVITE_NEW_REG'								=> 'New registration',
	'ACP_INVITE_NEW_REG_EXPLAIN'						=> 'Number of invitations given to a new user once registered.',
	'ACP_INVITE_NO_ANONYMOUS'							=> 'Guest users can not be selected!',
	'ACP_INVITE_NO_GROUP'								=> 'No accessible group. If you want to create a new group can do it %sHERE%s.',
	'ACP_INVITE_NO_GROUP_SPECIFIED'						=> 'You didn’t select a group.',
	'ACP_INVITE_NO_PETITION'							=> 'There is currently no request for invitation!',
	'ACP_INVITE_NO_MARKED'								=> 'You have not selected a user either!',
	'ACP_INVITE_NO_PEND_INVITE'							=> 'There are currently no pending invitations.',
	'ACP_INVITE_NO_SELECT_INVITE'						=> 'You didn’t select an invitation!',
	'ACP_INVITE_NO_USERS'								=> 'Currently no received invitations!',
	'ACP_INVITE_NO_VALUE'								=> 'You didn’t set the number of invitations!',
	'ACP_INVITE_NUMBER_INVITED_USERS'					=> 'Number of invited users',
	'ACP_INVITE_PENDING'								=> 'Pending invitations',
	'ACP_INVITE_PENDING_EXPLAIN'						=> 'Here you can view and delete unused invitations sent out.',
	'ACP_INVITE_PEND_DATE'								=> 'Shipping time',
	'ACP_INVITE_PEND_DB'								=> 'Outstanding invitations',
	'ACP_INVITE_PEND_DELETE_ALL_SUCCESSES'				=> 'All pending invitations have been successfully deleted.',
	'ACP_INVITE_PEND_DELETE_MARKS_SUCCESSES'			=> 'The selected pending invitations have been successfully deleted.',
	'ACP_INVITE_PEND_DELETE_MARK_SUCCESSES'				=> 'The selected pending invitation has been successfully deleted.',
	'ACP_INVITE_PEND_EMAIL'								=> 'Recipient e-mail address',
	'ACP_INVITE_PEND_END_DATE'							=> 'Expiration date',
	'ACP_INVITE_PETITION_NSUCCESS'						=> 'The rejection was successful.',
	'ACP_INVITE_PETITION_NSUCCESSES'					=> 'The rejections were successful.',
	'ACP_INVITE_PETITION_YSUCCESS'						=> 'The allocation has been successfully completed.',
	'ACP_INVITE_PETITION_YSUCCESSES'					=> 'The allocations have been successfully completed.',
	'ACP_INVITE_NOTIFICATION_NO_USER'					=> 'The user name for sending notifications does not exist!',
	'ACP_INVITE_NOTIFICATION_USERNAME'					=> 'Notifications sender name',
	'ACP_INVITE_NOTIFICATION_USERNAME_EXPLAIN'			=> 'Name of the user who will send the notification.',
	'ACP_INVITE_POST_LIMIT_DB'							=> 'Total posts',
	'ACP_INVITE_POST_LIMIT_DB_EXPLAIN'					=> 'This is the post count needed to send out invitations.<br /><em>(0=disabled.)</em>',
	'ACP_INVITE_REG_EMAIL'								=> 'Registered e-mail',
	'ACP_INVITE_REQUEST_CAUSE'							=> 'Activation limit',
	'ACP_INVITE_REQUEST_CAUSE_EXPLAIN'					=> 'When claiming more than this number of invitations, a reason is required to continue.<br /><em>(0=disabled.)</em>',
	'ACP_INVITE_REQUEST_MAX'							=> 'Maximum number of invitations available',
	'ACP_INVITE_REQUEST_MAX_EXPLAIN'					=> '<em>(0=disable requests.)</em>',
	'ACP_INVITE_REQUIRE_AC_DISABLED'					=> 'Registration by invitation is not possible, since it is currently turned off!',
	'ACP_INVITE_SORT_NAME'								=> 'Invited',
	'ACP_INVITE_SYNC_DATA'								=> 'Resynchronise total data',
	'ACP_INVITE_SYNC_DATA_CONFIRM'						=> 'Are you sure you want to resynchronise the invite totals data?',
	'ACP_INVITE_SYNC_DATA_EXPLAIN'						=> 'The invite total data will be resynchronised, this includes the following: invited users, pending invitations and invitations claimed.',
	'ACP_INVITE_SYNC_DATA_SUCCESSES'					=> 'The invite total data was successfully resynchronised.',
	'ACP_INVITE_SYNC_USERS'								=> 'Ressincronize os dados dos usuários.',
	'ACP_INVITE_SYNC_USERS_CONFIRM'						=> 'Tem certeza de que deseja ressincronizar os dados dos usuários convidados?',
	'ACP_INVITE_SYNC_USERS_EXPLAIN'						=> 'Os dados dos usuários convidados serão ressincronizados',
	'ACP_INVITE_SYNC_USERS_SUCCESSES'					=> 'Os dados dos usuários convidados foram ressincronizados com sucesso.',
	'ACP_INVITE_TOTAL_USERS'							=> 'Número de usuários convidados',
	'ACP_INVITE_TOTAL_USERS_DAY'						=> 'Número de usuários convidados por dia',
	'ACP_INVITE_UG_TASK'								=> 'Selecione uma tarefa',
	'ACP_INVITE_USERS'									=> 'Usuários que convidaram',
	'ACP_INVITE_USERS_COUNT'							=> 'Solicitações totais (%s)',
	'ACP_INVITE_USERS_EXPLAIN'							=> 'Aqui você pode ver a lista de usuários que já convidaram um novo membro para o site.',
	'ACP_INVITE_USERS_MANAGEMENT'						=> 'Gerenciar usuários selecionados',
	'ACP_INVITE_USERS_NO_GROUP'							=> 'Não há usuário no grupo selecionado.',
	'ACP_INVITE_USERS_NO_GROUPS'						=> 'Não há usuário nos grupos selecionados.',
	'ACP_INVITE_USER_EXPLAIN'							=> 'O valor mudará a quantidade de convites do usuário.',
	'ACP_INVITE_USER_LIST'								=> 'Número de usuários por página',
	'ACP_INVITE_USER_LIST_EXPLAIN'						=> 'Este valor define o número de usuários listados em uma página.',
	'ACP_INVITE_USER_MANAGEMENT'						=> 'Um gerenciamento de usuários',
	'ACP_INVITE_USER_MANAGEMENT_EXPLAIN'				=> 'Aqui você pode alterar a quantidade de convites do usuário.',
	'ACP_INVITE_USER_NAME'								=> 'nome',

	'LOG_INVITE_CONFIG'							=> '<strong>Configuração de convite modificada.</strong>',
	'LOG_INVITE_DELETE'							=> '<strong>Convite Revogado.</strong><br />» Endereço de e-mail: %s',
	'LOG_INVITE_SIZE_DELETE'					=> '<strong>Convite recusado</strong><br />» Usuário: %s',
	'LOG_INVITE_INSTALL'						=> '<strong>phpBB Invite instalado</strong><br />» Versão: %s',
	'LOG_INVITE_HANDLING_GROUPS_DELETE'			=> '<strong>Convites excluídos de um grupo.</strong><br />» <b>Grupo:</b> (<em>%s</em>).',
	'LOG_INVITE_HANDLING_GROUPS_DELETES'		=> '<strong>Convites excluídos de grupos.</strong><br />» <b>Grupos:</b> (<em>%s</em>).',
	'LOG_INVITE_HANDLING_GROUPS_IG'				=> '<strong>Convite adicionado para um grupo.</strong><br />» <b>Grupo:</b> (<em>%s</em>)<br />» Convite adicionado: (%s).',
	'LOG_INVITE_HANDLING_GROUPS_IGS'			=> '<strong>Convite adicionado para grupos.</strong><br />» <b>Grupos:</b> (<em>%s</em>)<br />» Convite adicionado: (%s).',
	'LOG_INVITE_HANDLING_GROUPS_ISG'			=> '<strong>Convites adicionados para um grupo.</strong><br />» <b>Grupo:</b> (<em>%s</em>)<br />» Convites adicionados: (%s).',
	'LOG_INVITE_HANDLING_GROUPS_ISGS'			=> '<strong>Convites adicionados para grupos.</strong><br />» <b>Grupos:</b> (<em>%s</em>)<br />» Convites adicionados: (%s)',
	'LOG_INVITE_HANDLING_USERS_DELETE'			=> '<strong>Convites excluídos de um usuário.</strong><br />» <b>Usuário:</b> (<em>%s</em>).',
	'LOG_INVITE_HANDLING_USERS_DELETES'			=> '<strong>Convites excluídos de usuários.</strong><br />» <b>Usuários:</b> (<em>%s</em>).',
	'LOG_INVITE_HANDLING_USERS_ISU'				=> '<strong>Convite adicionado para um usuário.</strong><br />» <b>Usuário:</b> (<em>%s</em>)<br />» Convites adicionados: (%s).',
	'LOG_INVITE_HANDLING_USERS_ISUS'			=> '<strong>Convites adicionados para usuários.</strong><br />» <b>Usuários:</b> (<em>%s</em>)<br />» Convites adicionados: (%s).',
	'LOG_INVITE_HANDLING_USERS_IU'				=> '<strong>Convite adicionado para um usuário.</strong><br />» <b>Usuário:</b> (<em>%s</em>)<br />» Convites adicionados: (%s).',
	'LOG_INVITE_HANDLING_USERS_IUS'				=> '<strong>Convites adicionados para usuários.</strong><br />» <b>Usuários:</b> (<em>%s</em>)<br />» Convites adicionados: (%s).',
	'LOG_INVITE_HANDLING_USER_I'				=> '<strong>Mudou a quantidade de convites.</strong><br />» <b>Usuário:</b> (<em>%s</em>)<br />» Nova quantidade: (%s) convite.',
	'LOG_INVITE_HANDLING_USER_IS'				=> '<strong>Mudou a quantidade de convites.</strong><br />» <b>Usuário:</b> (<em>%s</em>)<br />» Nova quantidade: (%s) convites.',
	'LOG_INVITE_ALLOCATE'						=> '<strong>Convites adicionados para o usuário %s.</strong><br />» Convites adicionados: %s.',

	'LOG_INVITE_SEND_INVITATION'				=> '<strong>Convite enviado.</strong><br />» %s',
	'LOG_INVITE_SEND_INVITATIONS'				=> '<strong>Convites enviados.</strong><br />» %s',

	'LOG_INVITE_PEND_ALL_DELETE'				=> '<strong>Excluiu todos os convites pendentes.</strong>',
	'LOG_INVITE_PEND_MARKED_DELETE'				=> '<strong>Excluído convite pendente selecionado.</strong><br />» Usuário: %s',
	'LOG_INVITE_PEND_MARKED_DELETES'			=> '<strong>Excluídos convites pendentes selecionados.</strong><br />» Usuários: %s',
	'LOG_INVITE_SYNC_DATA'						=> '<strong>Ressincronização de dados de convite.</strong>',
	'LOG_INVITE_SYNC_USERS'						=> '<strong>Ressincronização de dados de convite do usuário.</strong>',
));
