<?php
/**
 *
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_USERNAME'				=> 'Change username',
	'ASS_TYPE_USERNAME_CONFIRM'		=> 'Are you sure you wish to change username?',
	'ASS_TYPE_USERNAME_LOG'			=> 'changed username',
	'ASS_TYPE_USERNAME_TITLE'		=> 'Username',
	'ASS_TYPE_USERNAME_SUCCESS'		=> 'You have successfully changed username.',

	'ASS_TYPE_USERNAME_NO_DATA'		=> 'This item type does not need any additional data.',
]);
