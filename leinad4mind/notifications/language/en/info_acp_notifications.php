<?php

/**
 *
 * @package		  Push Notifications
 * @copyright (c) 2017 - 2022 Leinad4Mind
 * @license		  http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB')) {
   exit;
}

if (empty($lang) || !is_array($lang)) {
   $lang = array();
}

$lang = array_merge($lang, array(
   'ACP_PUSH_TITLE'            => 'Browser Notifications',
   'WEB_PUSH_NOTICE'            => '<div class="phpinfo"><p>The settings for this extensions are in <strong>%1$s » %2$s » %3$s</strong>.</p></div>',
   'LOG_WEB_PUSH_GENERAL_ERROR'   => '<strong>Web Push error</strong><br />» %s',
   'LOG_WEB_PUSH_SERVER_ERROR'      => '<strong>Web Push error</strong> with code <strong>%1$s</strong><br />» %2$s',
));
