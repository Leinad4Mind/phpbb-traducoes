<?php
/**
 *
 * BBajaxnews. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.0] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2018, SiteSplat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	// special
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_BBDONATION'			=> '%s acabou de fazer uma doação.',
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_BBMEMBERSHIP'		=> '%s comprou membership LV%s.',
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_BBVOTE'				=> 'A melhor resposta foi marcada em “%s”.',
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_FRIEND_LOGGED_IN'	=> 'Seu amigo %s acabou de logar.', 
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_NEW_ANNOUNCEMENT'	=> '%s acaba de postar um novo anúncio “%s” em “%s”.',
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_NEW_STICKY'			=> '%s acabou de tornar o tópico "%s" fixo em "%s"', 
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_NEW_TOPIC'			=> '%s acabou de postar um novo tópico “%s” em “%s”.', 
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_USER_JOINED_BOARD'	=> '%s acabou de se cadastrar no site.',
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_USER_JOINED_GROUP'	=> '%s acabou de entrar no grupo %s.',

	// casual
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_NEW_POSTS_TODAY'	=> [
		0	=> 'Nenhum post foi postado hoje.',
		1	=> 'Hoje foi postado 1 post.', 
		2	=> 'Hoje foram postados %d posts.',
	],
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_USERS_ONLINE_24H'	=> [
		0	=> 'Nenhum usuário esteve online na última %dh.',
		1	=> 'Teve um usuário online na última %2$dh.',
		2	=> 'Tivemos %d usuários online nas últimas %dh.',
	],
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_USERS_ONLINE'	=> [
		0	=> 'Não há ninguém online neste momento.',
		1	=> 'Atualmente, há 1 usuário online neste momento.',
		2	=> 'Atualmente, há %d usuários online neste momento.',
	],
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_RANDOM_POLL_NONE'	=> 'Você votou em todas as enquetes disponíveis. legal!',
	'SITESPLAT_BBAJAXNEWS_AJAXNEWS_TYPE_RANDOM_POLL'		=> 'Há uma enquete “%s” esperando pela sua resposta.', 
));
