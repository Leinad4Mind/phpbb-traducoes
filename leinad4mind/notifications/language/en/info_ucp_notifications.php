<?php

/**
 *
 * @package		  Push Notifications
 * @copyright (c) 2017 - 2022 Leinad4Mind
 * @license		  http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB')) {
   exit;
}

if (empty($lang) || !is_array($lang)) {
   $lang = array();
}

$lang = array_merge($lang, array(
   'LEINAD4MIND_NOTIFICATIONS_NOTIFICATION_METHOD_BROWSER' => 'Browser',
));
