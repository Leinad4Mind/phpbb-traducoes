<?php

/**
*
* @package BB3NewGroupUserPM
* @copyright (c) 2016 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'GROUPUSERPM_SETTINGS' => 'Private message when joining a group',
	'BB3NEWGROUPUSERPM_USERID' => 'User ID',
	'BB3NEWGROUPUSERPM_USERID_EXPLAIN' => 'The user ID on behalf of which private messages will be sent (0 - on behalf of the person who includes the group, this option will be common for all groups)',
	'BB3NEWGROUPUSERPM_SUBJECT' => 'Message subject',
	'BB3NEWGROUPUSERPM_SUBJECT_EXPLAIN' => 'It can be in the form of text or a variable name (do not use HTML code), the variable must be defined in the file /ext/ppk/bb3newgroupuserpm/language/Language/acp_bb3newgroupuserpm.php',
	'BB3NEWGROUPUSERPM_MESSAGE' => 'Message text',
	'BB3NEWGROUPUSERPM_MESSAGE_EXPLAIN' => 'To send users a private message when joining this group, may be in the form of text or a variable name, the variable should be defined in the file /ext/ppk/bb3newgroupuserpm/language/Language/acp_bb3newgroupuserpm.php, you can use , as well as the following variables: <br /> {USER_ID} - user ID <br /> {USERNAME} - username <br /> {GROUP_ID} - group ID <br /> {GROUP_NAME} - group name <br /> to send a message, this field and the field below must be filled in, ',
	'GROUP_MESSAGE0_SUBJECT' => 'Example message subject',
	'GROUP_MESSAGE0_MESSAGE' => 'Sample text',
));
