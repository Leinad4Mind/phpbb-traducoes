<?php
/**
*
* @package Ban Hammer
* @copyright (c) 2015 phpBB Modders <https://phpbbmodders.net/>
* @author Rich McGirr (RMcGirr83)
* @author Jari Kanerva
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'BANNED_ERROR'		=> 'Havia um erro!',
	'BANNED_SUCCESS'	=> 'Todas as ações foram realizadas corretamente.',

	'ERROR_BAN_EMAIL'	=> 'Banning the email failed.',
	'ERROR_BAN_IP'		=> 'Banning the IP failed.',
	'ERROR_BAN_USER'	=> 'Banning the users name failed.',
	'ERROR_DEL_POSTS'	=> 'Deleting the users posts failed.',
	'ERROR_MOVE_GROUP'	=> 'Moving user to the selected group failed.',
	'ERROR_SFS'			=> 'Error with reporting to the Stop Forum Spam database',

	'BH_BAN_EMAIL'			=> 'Banir este endereço de email dos usuários',
	'BH_BAN_GIVE_REASON'	=> 'Razão para o banimento mostrada ao banido',
	'BH_BAN_IP'				=> 'Banir este endereço de IP de usuários',
	'BH_BAN_IP_EXPLAIN'		=> '<strong>Cuidado com isso.</strong>A maioria dos usuários domésticos tem endereços IP dinâmicos e só precisa reiniciar seu roteador/modem para obter um novo endereço IP. No dia seguinte, esse endereço IP pode ser atribuído a um usuário que você deseja em seu site. Os spammers também usam proxies de anonimato na internet ou a rede Tor, fazendo uma proibição de IP sem sentido.',
	'BH_BAN_LENGTH'			=> 'Banir este usuário para %s',
	'BH_BAN_REASON'			=> 'Razão interna para esse banimento',
	'BH_BAN_USER'			=> 'Banir este usuário para %s',
	'BH_BAN_USER_PERM'		=> 'Banir este nome de usuário permanentemente',
	'BH_BAN_EMAIL_PERM'		=> 'Banir este endereço de email dos usuários permanentemente',
	'BH_BAN_EMAIL_FOR'		=> 'Banir este endereço de email dos usuários para %s',
	'BH_BAN_IP_PERM'		=> 'Banir este endereço de IP dos usuários permanentemente',
	'BH_BAN_IP_FOR'			=> 'Banir este endereço de IP dos usuários para %s',
	'BH_BANNED'				=> 'Este usuário esta banido',

	'BH_DEL_AVATAR'		=> 'Excluir o avatar deste usuário',
	'BH_DEL_PRIVMSGS'	=> 'Excluir mensagens privadas deste usuário', 
	'BH_DEL_POSTS'		=> 'Excluir os posts deste usuário',
	'BH_DEL_PROFILE'	=> 'Excluir os campos de perfil deste usuário',
	'BH_DEL_SIGNATURE'	=> 'Excluir a assinatura deste usuário',

	'BH_MOVE_GROUP'	=> 'Mover este usuário para o grupo &quot;%s&quot;', // %s will be a group name

	'BH_REASON'		=> 'Razão interna &quot;%s&quot;', // %s will be the reason
	'BH_REASON_USER'	=> 'Razão para o usuário &quot;%s&quot;', // %s will be the reason

	'BH_SUBMIT_SFS'	=> 'Enviar ao stop forum spam',

	'BH_THIS_USER'	=> 'Banir este usuário',

	'SFS_REPORT'	=> 'Denunciar este usuário ao Stop Forum Spam',
	'SURE_BAN'		=> 'Tem certeza de que deseja banir <strong>%s</strong>?', // %s will be a username.

	'THIS_WILL'	=> 'Isso vai',
));
