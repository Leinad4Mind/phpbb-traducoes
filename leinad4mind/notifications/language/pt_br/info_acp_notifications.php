<?php

/**
 *
 * @package		  Push Notifications
 * @copyright (c) 2017 - 2018 Leinad4Mind
 * @license		  http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * Brazilian Portuguese translation by eunaumtenhoid [2018][ver 1.2.0] (https://github.com/phpBBTraducoes)
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_PUSH_TITLE'				=> 'Notificações do navegador',
	'WEB_PUSH_NOTICE'				=> '<div class="phpinfo"><p>As Configurações para esta extensão estão em <strong>%1$s » %2$s » %3$s</strong>.</p></div>',
	'LOG_WEB_PUSH_GENERAL_ERROR'	=> '<strong>Erro no Web Push</strong><br />» %s',
	'LOG_WEB_PUSH_SERVER_ERROR'		=> '<strong>Erro no Web Push</strong> com o código <strong>%1$s</strong><br />» %2$s',
));
