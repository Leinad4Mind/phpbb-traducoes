<?php
/**
 *
 * phpBB Studio - Advanced Points System. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.0] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ACP_APS_POINTS_COPY'				=> 'Copiar %s de',
	'ACP_APS_POINTS_COPY_EMPTY_FROM'	=> 'Você precisa selecionar um fórum "de"',
	'ACP_APS_POINTS_COPY_EXPLAIN'		=> 'Se você optar por copiar %1$s, o fórum terá o mesmo %1$s que você selecionou aqui. Isso substituirá qualquer %1$s que você configurou anteriormente para este fórum com o %1$s do fórum selecionado aqui. Se nenhum fórum for selecionado, o %1$s atual será mantido.',
	'ACP_APS_POINTS_COPY_NOT'			=> 'Não copiar %s',
	'ACP_APS_POINTS_COPY_SUCCESS'		=> 'Você copiou o %s com sucesso.',
	'ACP_APS_POINTS_COPY_TITLE'			=> 'Copiar %s',

	'ACP_APS_POINTS_RESET'				=> 'Reset %s',
	'ACP_APS_POINTS_RESET_CONFIRM'		=> 'Are you sure you wish to to reset the %s for this forum?',
	'ACP_APS_POINTS_RESET_EXPLAIN'		=> 'If you select to reset %1$s, all values for this forum will be set to 0. This will overwrite any %1$s you have previously set for this forum or any forum you selected below to copy %1$s from.',
	'ACP_APS_POINTS_RESET_SUCCESS'		=> 'You have successfully reset the %s.',

	'APS_POINTS_DIFF'					=> 'Diferença de %s',
	'APS_POINTS_OLD'					=> '%s antigos',
	'APS_POINTS_NEW'					=> '%s novos',

	# Global
	'APS_POINTS_REGISTER'				=> 'Registrado',
	'APS_POINTS_BIRTHDAY'				=> 'Comemoraram seu aniversário',
	'APS_POINTS_BIRTHDAY_DESC'			=> 'Essa ação é executada no cron do sistema uma vez por dia. <br /><samp>ACP &raquo; Geral &raquo; Configurações do servidor &raquo; Executar tarefas periódicas a partir do cron do sistema', 
	'APS_POINTS_MOD_WARN'				=> 'Advertiu um usuário',
	'APS_POINTS_USER_WARN'				=> 'Recebeu uma advertência',
	'APS_POINTS_PM'						=> 'Criou uma mensagem privada',
	'APS_POINTS_PM_PER_RECIPIENT'		=> 'Por destinatário',

	# Misc
	'ACP_APS_POINTS_MISC'				=> 'Diversos',
	'APS_POINTS_PER_VOTE'				=> 'Por opção votada para',
	'APS_POINTS_VOTE_ADDED'				=> 'Votado em uma enquete',
	'APS_POINTS_VOTE_REMOVED'			=> 'Removido um voto',
	'APS_POINTS_VOTE_AMOUNT'			=> 'Quantidade de opções votadas',

	# Topics / Posts
	'APS_POINTS_POST'					=> 'Criou um post',
	'APS_POINTS_TOPIC'					=> 'Criou um tópico',
	'APS_POINTS_STICKY'					=> 'Created a sticky',
	'APS_POINTS_ANNOUNCE'				=> 'Created an announcement',
	'APS_POINTS_GLOBAL'					=> 'Created a global announcement',
	'APS_POINTS_PER_CHAR'				=> 'Per character',
	'APS_POINTS_PER_CHAR_DESC'			=> 'The text is stripped from BBCodes before counting the characters.',
	'APS_POINTS_PER_WORD'				=> 'Per word',
	'APS_POINTS_PER_WORD_DESC'			=> 'The text is stripped from BBCodes before counting the words.',
	'APS_POINTS_ATTACH_HAS'				=> 'Including attachment(s)',
	'APS_POINTS_ATTACH_PER'				=> 'Per included attachment',
	'APS_POINTS_PER_QUOTE'				=> 'Per quote',
	'APS_POINTS_PER_QUOTE_DESC'			=> 'Only the outer most quotes are counted and only if there is an author provided.',
	'APS_POINTS_POLL_HAS'				=> 'Included a poll',
	'APS_POINTS_POLL_OPTION'			=> 'Per included poll option',
	'APS_POINTS_EDIT'					=> 'Edited their post',
	'APS_POINTS_DELETE'					=> 'Deleted their post',
	'APS_POINTS_DELETE_SOFT'			=> 'Soft deleted their post',
	'APS_POINTS_BUMP'					=> 'Bumped a topic',

	# Topic types
	'ACP_APS_TOPIC_TYPES'				=> 'Topic types',

	'APS_POINTS_MOD_NORMAL_STICKY'		=> 'Made a topic a sticky',
	'APS_POINTS_MOD_NORMAL_ANNOUNCE'	=> 'Made a topic an announcement',
	'APS_POINTS_MOD_NORMAL_GLOBAL'		=> 'Made a topic a global announcement',
	'APS_POINTS_MOD_STICKY_NORMAL'		=> 'Made a sticky a normal topic',
	'APS_POINTS_MOD_STICKY_ANNOUNCE'	=> 'Made a sticky an announcement',
	'APS_POINTS_MOD_STICKY_GLOBAL'		=> 'Made a sticky a global announcement',
	'APS_POINTS_MOD_ANNOUNCE_NORMAL'	=> 'Made an announcement a normal topic',
	'APS_POINTS_MOD_ANNOUNCE_STICKY'	=> 'Made an announcement a sticky',
	'APS_POINTS_MOD_ANNOUNCE_GLOBAL'	=> 'Made an announcement a global announcement',
	'APS_POINTS_MOD_GLOBAL_NORMAL'		=> 'Made a global announcement a normal topic',
	'APS_POINTS_MOD_GLOBAL_STICKY'		=> 'Made a global announcement a sticky',
	'APS_POINTS_MOD_GLOBAL_ANNOUNCE'	=> 'Made a global announcement an announcement',

	'APS_POINTS_USER_NORMAL_STICKY'		=> 'Their topic was made a sticky',
	'APS_POINTS_USER_NORMAL_ANNOUNCE'	=> 'Their topic was made an announcement',
	'APS_POINTS_USER_NORMAL_GLOBAL'		=> 'Their topic was made a global announcement',
	'APS_POINTS_USER_STICKY_NORMAL'		=> 'Their sticky was made a normal topic',
	'APS_POINTS_USER_STICKY_ANNOUNCE'	=> 'Their sticky was made an announcement',
	'APS_POINTS_USER_STICKY_GLOBAL'		=> 'Their sticky was made a global announcement',
	'APS_POINTS_USER_ANNOUNCE_NORMAL'	=> 'Their announcement was made a normal topic',
	'APS_POINTS_USER_ANNOUNCE_STICKY'	=> 'Their announcement was made a sticky',
	'APS_POINTS_USER_ANNOUNCE_GLOBAL'	=> 'Their announcement was made a global announcement',
	'APS_POINTS_USER_GLOBAL_NORMAL'		=> 'Their global announcement was made a normal topic',
	'APS_POINTS_USER_GLOBAL_STICKY'		=> 'Their global announcement was made a sticky',
	'APS_POINTS_USER_GLOBAL_ANNOUNCE'	=> 'Their global announcement was made an announcement',

	# Moderation
	'APS_POINTS_MOD_COPY'				=> 'Copiou um tópico deste fórum',
	'APS_POINTS_USER_COPY'				=> 'O tópico dele foi copiado deste fórum',

	'APS_POINTS_MOD_CHANGE'				=> 'Mudou o autor de um post',
	'APS_POINTS_USER_CHANGE_FROM'		=> 'Removido como autor de um post',
	'APS_POINTS_USER_CHANGE_TO'			=> 'Tornou-se autor de um post',

	'APS_POINTS_MOD_DELETE_POST'		=> 'Excluiu um post',
	'APS_POINTS_USER_DELETE_POST'		=> 'Their post got deleted',
	'APS_POINTS_MOD_DELETE_SOFT_POST'	=> 'Soft deleted a post',
	'APS_POINTS_USER_DELETE_SOFT_POST'	=> 'Their post got soft deleted',
	'APS_POINTS_MOD_DELETE_TOPIC'		=> 'Deleted a topic',
	'APS_POINTS_USER_DELETE_TOPIC'		=> 'Their topic got deleted',
	'APS_POINTS_MOD_DELETE_SOFT_TOPIC'	=> 'Soft deleted a topic',
	'APS_POINTS_USER_DELETE_SOFT_TOPIC'	=> 'Their topic got soft deleted',

	'APS_POINTS_MOD_EDIT'				=> 'Editou um post',
	'APS_POINTS_USER_EDIT'				=> 'O post deles foi editado',

	'APS_POINTS_MOD_LOCK'				=> 'Locked a topic',
	'APS_POINTS_USER_LOCK'				=> 'Their topic got locked',
	'APS_POINTS_MOD_LOCK_POST'			=> 'Locked a post',
	'APS_POINTS_USER_LOCK_POST'			=> 'Their post got locked',
	'APS_POINTS_MOD_UNLOCK'				=> 'Unlocked a topic',
	'APS_POINTS_USER_UNLOCK'			=> 'Their topic got unlocked',
	'APS_POINTS_MOD_UNLOCK_POST'		=> 'Unlocked a post',
	'APS_POINTS_USER_UNLOCK_POST'		=> 'Their post got unlocked',

	'APS_POINTS_MOD_MERGE'				=> 'Merged a topic',
	'APS_POINTS_MOD_MERGE_DESC'			=> 'This will also trigger the “delete” action on the topics that are being merged into an other.',
	'APS_POINTS_USER_MERGE'				=> 'Their topic got merged',
	'APS_POINTS_USER_MERGE_DESC'		=> 'This will also trigger the “delete” action on the topics that are being merged into an other.',

	'APS_POINTS_MOD_MOVE_POST'			=> 'Moved a post',
	'APS_POINTS_MOD_MOVE_POST_DESC'		=> 'Moved values are for moving <strong>from</strong> this forum, not <em>to</em>.',
	'APS_POINTS_USER_MOVE_POST'			=> 'Their post got moved',
	'APS_POINTS_MOD_MOVE_TOPIC'			=> 'Moved a topic',
	'APS_POINTS_USER_MOVE_TOPIC'		=> 'Their topic got moved',

	'APS_POINTS_MOD_APPROVE'			=> 'Approved a post',
	'APS_POINTS_MOD_DISAPPROVE'			=> 'Disapproved a post',
	'APS_POINTS_MOD_RESTORE'			=> 'Restored a post',
	'APS_POINTS_USER_APPROVE'			=> 'Their post is approved',
	'APS_POINTS_USER_DISAPPROVE'		=> 'Their post is disapproved',
	'APS_POINTS_USER_RESTORE'			=> 'Their post is restored',

	'APS_POINTS_USER_ADJUSTED'			=> 'Ajustado pelo moderador',
]);
