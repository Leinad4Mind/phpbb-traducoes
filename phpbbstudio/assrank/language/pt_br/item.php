<?php
/**
 *
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.0] (https://github.com/phpBBTraducoes)
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_RANK'					=> 'Associar rank',
	'ASS_TYPE_RANK_CONFIRM'			=> 'Tem certeza de que deseja associar esse rank?',
	'ASS_TYPE_RANK_LOG'				=> 'associou um rank',
	'ASS_TYPE_RANK_TITLE'			=> 'Rank',
	'ASS_TYPE_RANK_SUCCESS'			=> 'Você associou este rank com sucesso.',
	'ASS_TYPE_RANK_NOT_EXIST'		=> 'O rank solicitado não existe.',
]);
