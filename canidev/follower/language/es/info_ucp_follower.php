<?php
/**
* [Spanish [Es]]
* @package cBB Follower
* @version 2.0.3 01/06/2022
*
* @copyright (c) 2022 CaniDev
* @license https://creativecommons.org/licenses/by-nc/4.0/
*/

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ABOUTME'					=> 'Acerca de mi',
	'ABOUTME_EXPLAIN'			=> 'Pequeño texto (máx 500 caracteres), visible por los demas usuarios.',
	'ERROR_ABOUTME_TOO_LONG'	=> 'El campo "Acerca de mí" contiene más caracteres de los permitidos',
	'HEADER_IMAGE'				=> 'Imagen de perfil',
	'HEADER_IMAGE_EXPLAIN'		=> 'Imagen que se mostrará como fondo de la cabecera de su perfil.',
	
	// Notifications
	'NOTIFICATION_GROUP_FOLLOWER'	=> 'Notificaciones del seguidor de usuarios',
	'FW_NOTIFICATION_TYPE_BIRTHDAY'	=> 'Alguien a quien sigue cumpla años',
	'FW_NOTIFICATION_TYPE_COMMENT'	=> 'Alguien publique un comentario en su muro',
	'FW_NOTIFICATION_TYPE_FOLLOW'	=> 'Alguien le siga',
	'FW_NOTIFICATION_TYPE_LIKE'		=> 'A alguien le guste un comentario o mensaje suyo',
	'FW_NOTIFICATION_TYPE_MENTION'	=> 'Alguien le mencione en un comentario o mensaje',
	'FW_NOTIFICATION_TYPE_POST'		=> 'Alguien a quien sigue publique un mensaje',
	'FW_NOTIFICATION_TYPE_REPLY'	=> 'Alguien responda a un comentario de su muro',
	'FW_NOTIFICATION_TYPE_SHARE'	=> 'Alguien comparta un comentario de su muro',
	'FW_NOTIFICATION_TYPE_TOPIC'	=> 'Alguien a quien sigue publique un tema',
));
