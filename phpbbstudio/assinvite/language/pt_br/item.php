<?php
/**
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_INVITE'				=> 'Reivindicar convites',
	'ASS_TYPE_INVITE_CONFIRM'		=> 'Tem certeza de que deseja reivindicar esses convites?',
	'ASS_TYPE_INVITE_LOG'			=> 'convites reivindicados',
	'ASS_TYPE_INVITE_TITLE'			=> 'Convites',
	'ASS_TYPE_INVITE_SUCCESS'		=> 'Você reivindicou esses convites com sucesso.',
	'ASS_TYPE_INVITE_ERROR'			=> 'A extensão “leinad4mind/invitation” não está ativada!',
	'ASS_TYPE_INVITE_EMPTY'			=> 'A quantidade de convites não pode ser zero.',
]);
