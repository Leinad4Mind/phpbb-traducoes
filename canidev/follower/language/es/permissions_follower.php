<?php
/**
* [Spanish [Es]]
* @package cBB Follower
* @version 2.0.3 01/06/2022
*
* @copyright (c) 2022 CaniDev
* @license https://creativecommons.org/licenses/by-nc/4.0/
*/

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACL_CAT_FOLLOWER'			=> 'Seguidos de Usuarios',
	
	'ACL_U_FWR_FOLLOW'			=> 'Puede seguir a usuarios',
	'ACL_U_FWR_INTERACTION'		=> 'Puede compartir y puntuar comentarios y mensajes',
	'ACL_U_FWR_MANAGE'			=> 'Puede editar/borrar los comentarios de su muro',
	'ACL_U_FWR_MENTION'			=> 'Puede mencionar a usuarios',
	'ACL_U_FWR_PUBLISH'			=> 'Puede publicar comentarios en su muro',
	
	'ACL_M_FOLLOWER_MANAGE'		=> 'Puede editar/borrar los comentarios de los usuarios',
));
