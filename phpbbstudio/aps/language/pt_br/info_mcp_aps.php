<?php
/**
 *
 * phpBB Studio - Advanced Points System. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.4] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'MCP_APS_POINTS'		=> '%s',
	'MCP_APS_CHANGE'		=> 'Alterar',
	'MCP_APS_FRONT'			=> 'Principal',
	'MCP_APS_LOGS'			=> 'Logs',

	'MCP_APS_LATEST_ADJUSTED'		=> 'Últimos %d ajustes', 
	'MCP_APS_USERS_TOP'				=> '%d maiores usuários',
	'MCP_APS_USERS_BOTTOM'			=> '%d menores usuários',

	'MCP_APS_POINTS_CURRENT'				=> '%s Atuais',
	'MCP_APS_POINTS_CHANGE'					=> 'Alterar %s',

	'MCP_APS_POINTS_USER_CHANGE'			=> 'Tem certeza de que deseja ajustar %s deste usuário?', 
	'MCP_APS_POINTS_USER_CHANGE_SUCCESS'	=> 'Os %s para este usuário foram ajustados com sucesso.',   
]);
