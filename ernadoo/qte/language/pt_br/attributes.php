<?php
/**
 *
 * @package Quick Title Edition Extension
 * @copyright (c) 2015 ABDev
 * @copyright (c) 2015 PastisD
 * @copyright (c) 2015 Geolim4 <http://geolim4.com>
 * @copyright (c) 2015 Zoddo <zoddo.ino@gmail.com>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * Brazilian Portuguese translation by eunaumtenhoid [ver 2.0.0-a2] (https://github.com/eunaumtenhoid)
 */

// ignore
if (!defined('IN_PHPBB'))
{
	exit;
}

// init lang ary, if it doesn't !
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
    'QTE_CAN_USE_ATTR'	=> 'Pode usar o tipo %s ',

	// select
	'QTE_ATTRIBUTES' => 'Tipos do tópico',
	'QTE_ATTRIBUTE' => 'Tipo do tópico',

	'QTE_ATTRIBUTE_ADD' => 'Selecione um tipo para o tópico',
	'QTE_ATTRIBUTE_REMOVE' => 'Remover o tipo do tópico',
	'QTE_ATTRIBUTE_DESIRED' => 'Selecionar o tipo desejado',
	'QTE_ATTRIBUTE_KEEP' => 'Manter o tipo atual',

	// notifications
	'QTE_ATTRIBUTE_ADDED' => 'Um Tipo foi aplicado ao título do tópico',
	'QTE_ATTRIBUTE_UPDATED' => 'O Tipo do tópico foi atualizado',
	'QTE_ATTRIBUTE_REMOVED' => 'O Tipo de tópico foi removido',

	'QTE_TOPIC_ATTRIBUTE_ADDED' => 'Um tipo foi aplicado ao tópico selecionado',
	'QTE_TOPICS_ATTRIBUTE_ADDED' => 'Um tipo foi aplicado aos tópicos selecionados',
	'QTE_TOPIC_ATTRIBUTE_UPDATED' => 'O tipo do tópico selecionado foi atualizado',
	'QTE_TOPICS_ATTRIBUTE_UPDATED' => 'Os tipos dos tópicos selecionados foram atualizados',
	'QTE_TOPIC_ATTRIBUTE_REMOVED' => 'O tipo do tópico selecionado foi removido',
	'QTE_TOPICS_ATTRIBUTE_REMOVED' => 'Os tipos dos tópicos selecionados foram removidos',

	// search
	'QTE_ATTRIBUTE_SELECT' => 'Selecionar um tipo de tópico',
	'QTE_ATTRIBUTE_SEARCH' => 'Pesquisar tipos',
	'QTE_ATTRIBUTE_SEARCH_EXPLAIN' => 'Selecione o tipo que você deseja pesquisar',

	// sort
	'QTE_SORT' => 'Como tipo',
	'QTE_ALL' => 'Todos',

	// mistake messages
	'QTE_ATTRIBUTE_UNSELECTED' => 'Você deve selecionar um tipo!',

	// placeholders
	'QTE_KEY_USERNAME' => '¦user¦',
	'QTE_KEY_DATE' => '¦date¦',
));

// topic attributes as keys
$lang = array_merge($lang, array(
	'QTE_SOLVED' => '[Resolvido por %mod% :: %date%]',
	'QTE_CANCELLED' => 'Cancelado',
));
