<?php
/**
 *
 * Ideas extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid (c) 2017 [ver 2.1.7] (https://github.com/phpBBTraducoes)
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
    exit;
}

if (empty($lang) || !is_array($lang))
{
    $lang = array();
}

$lang = array_merge($lang, array(
    'ADD'                	=> 'Adicionar',
	'ALL_IDEAS'				=> 'Todos pedidos',
	'ALREADY_VOTED'			=> 'Você já votou neste pedido.',

	'CHANGE_STATUS'			=> 'Alterar o status',
	'CLICK_TO_VIEW'			=> 'Clique para ver os votos.',
	'CONFIRM_DELETE'		=> 'Tem certeza de que deseja excluir esse pedido?',

	'DATE'					=> 'Data',
	'DELETE_IDEA'			=> 'Excluir pedido',
	'DUPLICATE'				=> 'Duplicado',

	'EDIT'					=> 'Editar',
	'ENABLE_JS'             => 'Por favor, habilite o JavaScript no seu navegador para usar o pedidos de forma eficaz.',

	'IDEAS'					=> 'Pedidos',
	'IDEA_DELETED'			=> 'Pedido excluido com sucesso.',
	'IDEA_ID'				=> 'ID do pedido',
	'IDEA_LIST'				=> 'Lista de pedidos',
	'IDEA_NUM'				=> 'pedido #',
	'IDEA_NOT_FOUND'		=> 'pedido não encontrado',
	'IDEAS_TITLE'			=> 'Pedidos',
	'IDEAS_NOT_AVAILABLE'	=> 'Os pedidos não estão disponíveis no momento.',
	'IMPLEMENTED'           => 'Atendido',
	'IMPLEMENTED_IDEAS'		=> 'Últimos pedidos atendidos',
	'IN_PROGRESS'           => 'Em progresso',
	'INVALID'				=> 'Inválido',
	'INVALID_VOTE'			=> 'Voto inválido; o número que você inseriu foi inválido.',

	'JS_DISABLED'           => 'O JavaScript está desativado',

	'LATEST_IDEAS'			=> 'Últimos pedidos abertos',
	'LOGGED_OUT'			=> 'Você deve estar logado para fazer isso.',

	'NEW'					=> 'Pendente',
	'NEW_IDEA'				=> 'Novo pedido',
	'NO_IDEAS_DISPLAY'		=> 'Não há pedidos para exibir.',

	'OPEN_IDEAS'			=> 'pedidos abertos',

	'POST_IDEA'				=> 'Postar um novo pedido',
	'POSTING_NEW_IDEA'		=> 'Postando um novo pedido',

    'REQUESTER'				=> 'Solicitante',
	'RATING'                => 'Avaliação',
	'REMOVE_VOTE'			=> 'Retirar meu voto',
	'RETURN_IDEAS'			=> '%sRetornar àos pedidos%s',
	'RFC'					=> 'RFC',
	'RFC_ERROR'				=> 'RFC deve ser um tópico na Área51.',

	'SEARCH_IDEAS'			=> 'Procura pedidos ...',
	'SCORE'                 => 'Ponto',
	'SHOW_W_STATUS'			=> 'Mostrar pedidos com status',
	'STATUS'				=> 'Status',

	'TICKET'				=> 'Ticket',
	'TICKET_ERROR'			=> 'Ticket ID deve ser do formato “PHPBB3-#####”.',
	'TICKET_ERROR_DUP'		=> 'Por favor use o número da ID do pedido.',
	'TITLE'					=> 'Título',
	'TITLE_TOO_LONG'		=> 'O assunto deve ter menos de %d caracteres de comprimento.',
	'TITLE_TOO_SHORT'		=> 'Você deve especificar um assunto ao postar um novo pedido.',
	'TOP'                   => 'Top',
	'TOP_IDEAS'				=> 'Top pedidos',
	'TOTAL_IDEAS'			=> array(
		1	=> '%d pedido',
		2	=> '%d pedidos',
	),
	'TOTAL_POINTS'			=> array(
		1	=> '%s ponto.',
		2	=> '%s pontos.',
	),

	'UPDATED_VOTE'			=> 'Votação atualizada com sucesso!',

	'VIEW_IDEA'				=> 'Ver Pedido',
	'VIEW_IMPLEMENTED'		=> 'Ver todos os pedidos atendidos',
	'VIEW_LATEST'			=> 'Veja todos os pedidos abertos',
	'VIEW_TOP'				=> 'Veja todos os pedidos mais votados',
	'VIEWING_IDEAS'			=> 'Visualizando pedidos',
	'VOTE'					=> 'Voto',
	'VOTE_DOWN'				=> 'Vote para baixo',
	'VOTE_ERROR'			=> 'Um erro ocorreu',
	'VOTE_FAIL'				=> 'Falha ao votar; verifique sua conexão.',
	'VOTE_SUCCESS'			=> 'Votou com sucesso neste pedido.',
	'VOTE_UP'				=> 'Vote para cima',
	'VOTES'					=> 'Votos',
));
