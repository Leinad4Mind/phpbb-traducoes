<?php
/**
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.0] (https://github.com/phpBBTraducoes)
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_REQUIRED'			=> 'A extensão <strong>“Advanced Shop System • Item type: Invite”</strong> exige que a extensão <strong>“Advanced Shop System”</strong> esteja ativada.',
	'INVITATION_REQUIRED'	=> 'A extensão <strong>“Advanced Shop System • Item type: Invite”</strong> exige que a extensão <strong>“Leinad4Mind: Invitation”</strong> esteja ativada.',	
]);
