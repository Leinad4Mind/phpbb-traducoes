<?php
/**
* [Spanish [Es]]
* @package cBB Follower
* @version 2.0.3 01/06/2022
*
* @copyright (c) 2022 CaniDev
* @license https://creativecommons.org/licenses/by-nc/4.0/
*/

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'COMMENT_ADD'			=> 'Nuevo comentario',

	'ADD_PHOTOS'			=> 'Añadir fotos',
	'COMMENTS'				=> 'Comentarios',
	'COPY_COMMENT_LINK'		=> 'Copiar enlace a este comentario',
	'EDIT'					=> 'Editar',
	'FOLLOW'				=> 'Seguir',
	'FOLLOWING'				=> 'Siguiendo',
	'FOLLOWING_BY'			=> 'Seguido por %s',
	'FOLLOWER'				=> 'Seguidor de Usuarios',
	'FOLLOWERS'				=> 'Seguidores',
	'FWR_HOME'				=> 'Cronología',
	'FWR_REPLY'				=> 'Responder',
	'FWR_REPLY_PLACEHOLDER'	=> 'Responder...',
	'LIKE'					=> 'Me gusta',
	'LIKE_UNDO'				=> 'No me gusta',
	'LOAD_MORE'				=> 'Cargar más',
	'MENTIONS'				=> 'Menciones',
	'MENTION_USER'			=> 'Mencionar usuario',
	'NO_COMMENT'			=> 'El comentario seleccionado no existe',
	'NO_ITEMS'				=> 'No hay elementos para mostrar',
	'NO_RESULTS'			=> 'No hay resultados para su búsqueda',
	'PUBLISH'				=> 'Publicar',
	'RECENT_ACTIVITY'		=> 'Actividad Reciente',
	'RESULTS_OF'			=> '<strong>Resultados de:</strong> <em>%s</em>',
	'RESULTS_USER'			=> '<strong>Usuarios:</strong> <em>%s</em>',
	'SAVE'					=> 'Guardar',
	'SEARCH_RESULTS'		=> 'Resultados de búsqueda',
	'SHARE'					=> 'Compartir',
	'SHARE_UNDO'			=> 'Dejar de compartir',
	'SHARED_BY'				=> 'Compartido por %s',
	'SOMEBODY'				=> 'Alguien',
	'TITLE_FOLLOWING'		=> 'Personas a las que sigue %s',
	'TITLE_FOLLOWERS'		=> 'Personas que siguen a %s',
	'TITLE_POSTS'			=> 'Mensajes publicados por %s',
	'TITLE_TOPICS'			=> 'Temas publicados por %s',
	'TITLE_WALL'			=> 'Muro de %s',
	'TRENDS'				=> 'Tendencias',
	'UNFOLLOW'				=> 'Dejar de seguir',
	'UNSHARE'				=> 'Dejar de compartir',
	'USER_INFO'				=> 'Acerca de %s',
	'USER_WALL'				=> 'Muro del usuario',
	'USUAL_POSTER'			=> 'Posteador habitual',
	'VIEW_ALL'				=> 'Ver todos',
	'VIEW_COMMENT_RESULTS'	=> 'Ver resultados de comentarios',
	'VISITED'				=> 'Última visita',
	'WALL'					=> 'Muro',
	'WALL_IN_REPLY'			=> 'En respuesta a &uarr;',
	'WALL_REPLIES'			=> 'Respuestas &darr;',
	'WHO_FOLLOW'			=> 'A quien seguir',
	'WRITE_TO_WALL'			=> 'Escribe algo a %s',
	'YEARS_OLD'				=> 'años',

	'EMPTY_MY_HOME'			=> 'Aquí se mostrará la actividad de las personas a las que sigue.',
	'EMPTY_MY_FOLLOWERS'	=> 'Aquí se mostrarán las personas que lo siguen.',
	'EMPTY_MY_FOLLOWING'	=> 'Aquí se mostrarán las personas a las que sigue.',
	'EMPTY_MY_MENTIONS'		=> 'Aquí se mostrarán los comentarios o mensajes en los que se menciona su nombre.',
	'EMPTY_MY_POSTS'		=> 'Aquí se mostrarán los mensajes que publique en el foro.',
	'EMPTY_MY_TOPICS'		=> 'Aquí se mostrarán los temas que publique en el foro.',
	'EMPTY_MY_WALL'			=> 'Este es su muro personal<br />Aquí se mostrarán sus comentarios.',
	'EMPTY_MY_WALL_NO_AUTH'	=> '<em>No tiene permisos para publicar comentarios</em>.',

	'EMPTY_OTHER_FOLLOWERS'	=> 'Nadie sigue a %s todavía.',
	'EMPTY_OTHER_FOLLOWING'	=> '%s aún no sigue a nadie.',
	'EMPTY_OTHER_POSTS'		=> '%s no ha escrito ningún mensaje todavía.',
	'EMPTY_OTHER_TOPICS'	=> '%s no ha creado ningún tema todavía.',
	'EMPTY_OTHER_WALL'		=> '%s aún no ha compartido nada.',
	
	'LINK_CLIPBOARD_ERROR'		=> 'No se ha podido copiar el enlace',
	'LINK_CLIPBOARD_SUCCESS'	=> 'Enlace copiado al portapapeles',

	'POST_ROW'			=> '<span style="font-weight:bold;font-size:0.9em;">Mensaje:</span> <a href="%1$s">%2$s</a><br /> <span class="row-small">%3$s</span>',
	'TOPIC_ROW'			=> '<span style="font-weight:bold;font-size:0.9em;">Tema:</span> <a href="%1$s">%2$s</a><br /><span class="row-small"> &raquo; En <a href="%3$s">%4$s</a></span>',
	
	// Notifications
	'FW_NOTIFICATION_BIRTHDAY'			=> '<strong>Hoy es el cumpleaños de %s</strong><br />Escriba algo en su muro para felicitarlo/a',
	
	'FW_NOTIFICATION_COMMENT'			=> array(
		1	=> '%s ha publicado un comentario en su muro',
		2	=> '%s han publicado un comentario en su muro',
	),

	'FW_NOTIFICATION_FOLLOW'			=> '%s ha comenzado a seguirle',
	'FW_NOTIFICATION_MENTION_COMMENT'	=> '%s le ha mencionado en un mensaje de su muro',
	'FW_NOTIFICATION_MENTION_POST'		=> '%s le ha mencionado en el tema:',
	
	'FW_NOTIFICATION_LIKE_COMMENT'	=> array(
		1	=> 'A %s le ha gustado un comentario suyo',
		2	=> 'A %s les ha gustado un comentario suyo',
	),
	
	'FW_NOTIFICATION_LIKE_POST'		=> array(
		1	=> 'A %s le ha gustado un mensaje que ha publicado',
		2	=> 'A %s les ha gustado un mensaje que ha publicado',
	),

	'FW_NOTIFICATION_REPLY'		=> '%s ha respondido a un comentario de su muro',
	
	'FW_NOTIFICATION_SHARE'			=> array(
		1	=> '%s ha compartido un comentario de su muro',
		2	=> '%s han compartido un comentario de su muro',
	),

	'SUBSCRIPTION_REMOVED'		=> 'La subscripción se ha eliminado con éxito.',
	
	// Install
	'EXT_INSTALL_ERROR'		=> 'Esta extensión no es compatible con alguna extensión instalada.<br />Revise la documentación para más información.',
	'CORE_INSTALL_ERROR'	=> 'No ha subido todos los archivos del paquete de instalación o está intentando instalar un paquete antiguo.<br />
		Por favor, asegúrese de subir todos los archivos (incluída la carpeta <em>core</em>) y de utilizar un paquete descargado de la web oficial.',
));
