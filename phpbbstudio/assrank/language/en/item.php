<?php
/**
 *
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_RANK'					=> 'Assign rank',
	'ASS_TYPE_RANK_CONFIRM'			=> 'Are you sure you wish to assign this rank?',
	'ASS_TYPE_RANK_LOG'				=> 'assigned a rank',
	'ASS_TYPE_RANK_TITLE'			=> 'Rank',
	'ASS_TYPE_RANK_SUCCESS'			=> 'You have successfully assigned this rank.',
	'ASS_TYPE_RANK_NOT_EXIST'		=> 'The requested rank does not exist.',
]);
