<?php
/**
 *
 * phpBB Studio - Advanced Points System. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.4] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'APS_DISABLE_EXTENDED'	=> 'Desativar o Advanced Points System não é possível, pois ele ainda está sendo estendido por outra extensão. Nome da extensão: %s',
	'APS_PHPBB_VERSION'		=> 'A versão mínima do phpBB necessária é %1$s, mas menor que %2$s',
	'APS_UP_INSTALLED'		=> 'A extensão "dmzx/ultimatepoints" não é compatível com esta!',
]);
