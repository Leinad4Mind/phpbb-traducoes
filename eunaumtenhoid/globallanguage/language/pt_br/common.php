<?php
/**
*
* global language extension for the phpBB Forum Software package.
*
* @copyright (c) 2018 eunaumtenhoid <https://github.com/phpBBTraducoes>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Create the lang array if it does not already exist
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// Merge the following language entries into the lang array
$lang = array_merge($lang, array(


	'TYPE_UPLOAD' 			    => 'Categoria do Upload',
	'SELECT_DESTINATION_UPLOAD' => 'Selecione o local onde sera postado este lançamento',
	'TOPIC_ICON' 			    => 'Status dos Links',
	'EDIT_REASON_REQUIRED'      => 'Edit reason is required!',
	
	// BBprefix
	'PREFIX_DROP_HERE'          => 'Status do projeto',
	'POSTING_PREFIXES'          => 'Status do projeto',
	'POSTING_PREFIXES_BOX'      => 'Status do projeto',
	
	
	// Groups
    'G_ADMINISTRATORS'					=> 'Administradores', 
	'G_BOTS'							=> 'BOTs', 
	'G_GUESTS'							=> 'Visitantes', 
	'G_REGISTERED'						=> 'Usuários', 
	'G_REGISTERED_COPPA'				=> 'Usuários da COPPA', 
	'G_GLOBAL_MODERATORS'				=> 'Moderadores',
	'G_NEWLY_REGISTERED'				=> 'Usuários recentes',	
	
	// Loja
	'OBSERVATION' 			    => 'Observação',
	'OBSERVATION_MESSAGE'       => 'Insira aqui a observação necessaria em relação ao produto comprado',
	'PRODUCT' 			        => 'Produto',
	'BUY' 			            => 'Fazer pedido',
	
	
	
));
