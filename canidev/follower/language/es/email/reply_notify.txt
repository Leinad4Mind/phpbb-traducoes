Subject: Nuevo respuesta a su comentario

Hola,

"{AUTHOR_NAME}" ha respondido a un comentario suyo en "{SITENAME}".

Para ver la respuesta, use este enlace:
{U_ITEM_LINK}

Para dejar de recibir este tipo de notificaciones, use este enlace:
{U_UNSUBSCRIBE}

{EMAIL_SIG}
