<?php
/**
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_REQUIRED'			=> 'The <strong>“Advanced Shop System • Item type: Invite”</strong> extensions requires that the <strong>“Advanced Shop System”</strong> extension is enabled.',
	'INVITATION_REQUIRED'	=> 'The <strong>“Advanced Shop System • Item type: Invite”</strong> extensions requires that the <strong>“Leinad4Mind: Invitation”</strong> extension is enabled.',
]);
