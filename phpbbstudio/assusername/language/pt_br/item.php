<?php
/**
 *
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.0] (https://github.com/phpBBTraducoes)
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_USERNAME'				=> 'Alterar nome de usuário',
	'ASS_TYPE_USERNAME_CONFIRM'		=> 'Tem certeza de que deseja alterar o nome de usuário?',
	'ASS_TYPE_USERNAME_LOG'			=> 'nome de usuário alterado',
	'ASS_TYPE_USERNAME_TITLE'		=> 'Nome de usuário',
	'ASS_TYPE_USERNAME_SUCCESS'		=> 'Você alterou o nome de usuário com sucesso.',

	'ASS_TYPE_USERNAME_NO_DATA'		=> 'Este tipo de item não precisa de dados adicionais.',
]);
