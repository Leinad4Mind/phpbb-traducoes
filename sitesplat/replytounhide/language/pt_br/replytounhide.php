<?php
/**
*
 * @package ReplyToUnhide
 * @copyright (c) 2016 SiteSplat <http://sitesplat.com>
 * @license Proprietary
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	// Hide BBCode
	'HIDE_ON'		=> '<i class="fa fa-lock"></i> Conteúdo Oculto',
	'HIDE_OFF'		=> '<i class="fa fa-unlock"></i> Conteúdo Liberado',
	'HIDE_EXPLAIN'	=> 'É necessário que esteja registado e que <a href="#thanks_button"><span class="btn-primary btn-sm">&nbsp;<i class="fa fa-thumbs-up"></i><strong>&nbsp;Agradeça</strong></span></a> o tópico para poder visualizar o conteúdo...',
	'HIDE_HELPLINE'	=> '[hide]{TEXT}[/hide]',
	'HIDE_THANKS'	=> 'Conteúdo Oculto (Agradece o tópico, não seja gringo!)'
));
