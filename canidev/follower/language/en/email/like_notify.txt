Subject: Your received a new Like

Hi,

"{AUTHOR_NAME}" has indicated that he likes a message from you in "{SITENAME}".

To see the message, use this link:
{U_ITEM_LINK}

To stop receiving these types of notifications, use this link:
{U_UNSUBSCRIBE}

{EMAIL_SIG}
