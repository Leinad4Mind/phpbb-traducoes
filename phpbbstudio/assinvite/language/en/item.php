<?php
/**
 * Copyright (C) phpBB Studio, https://www.phpbbstudio.com - All Rights Reserved
 *
 * Unauthorized copying of file(s) or the whole package, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'ASS_TYPE_INVITE'				=> 'Claim invitations',
	'ASS_TYPE_INVITE_CONFIRM'		=> 'Are you sure you wish to claim these invitations?',
	'ASS_TYPE_INVITE_LOG'			=> 'claimed invitations',
	'ASS_TYPE_INVITE_TITLE'			=> 'Invitations',
	'ASS_TYPE_INVITE_SUCCESS'		=> 'You have successfully claimed these invitations.',
	'ASS_TYPE_INVITE_ERROR'			=> 'The extension “leinad4mind/invitation” is not enabled!',
	'ASS_TYPE_INVITE_EMPTY'			=> 'The amount of invitations can not be zero.',
]);
