<?php
/**
 *
 * phpBB Studio - Advanced Points System. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.4] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'APS_OVERVIEW'	=> 'Visão geral',
	'APS_SUCCESS'	=> 'Sucesso',
	'APS_TOP_USERS'	=> 'Top usuários',

	'APS_ADJUST_USER_POINTS'	=> 'Ajustar %s do usuário ',

	'APS_PURCHASE_POINTS'		=> 'Comprar %s',

	'APS_POINTS_ACTION'					=> 'Ações de %s',
	'APS_POINTS_ACTION_SEARCH'			=> 'Pesquisar ações de %s',
	'APS_POINTS_ACTION_TIME'			=> 'Tempo das ações de %s',
	'APS_POINTS_ACTIONS'				=> 'Ações de %s',
	'APS_POINTS_ACTIONS_ALL'			=> 'Todas as ações de %s', 
	'APS_POINTS_ACTIONS_NONE'			=> 'Ainda não há ações de %s.', 
	'APS_POINTS_ACTIONS_PAGE'			=> 'Ações por página',
	'APS_POINTS_ACTIONS_TOTAL'			=> [
		1 => '%2$d ação de %1$s ',
		2 => '%2$d ações de %1$s ',
	],

	'APS_POINTS_BLOCK_ADD'			=> 'Bloco de %s foi adicionado!',
	'APS_POINTS_BLOCK_DELETE'		=> 'Bloco de %s foi removido!!',
	'APS_POINTS_BLOCK_MOVE'			=> 'Bloco de %s foi movido!',
	'APS_POINTS_BLOCK_NO'			=> 'Sem blocos',
	'APS_POINTS_BLOCK_NONE'			=> 'Parece que você não possui nenhum bloco adicionado.',
	'APS_POINTS_BLOCK_NO_CONTENT'	=> 'Opa! Parece que algo deu errado.<br/>Este bloco não possui conteúdo!<br /><br />O <code>{% block content%} necessário...{% endblock%}</code> é obrigatório ausência de!',
	
	'APS_POINTS_FORMAT'	=> 'Formato de %s',

	'APS_POINTS_MAX'	=> 'Máximo de %s',
	'APS_POINTS_MIN'	=> 'Mínimo de %s',

	'APS_POINTS_NAME'	=> 'Nome',

	'APS_POINTS_DATA_EMPTY'	=> 'Não há dados de %s para exibir', 
	'APS_POINTS_GAINED'		=> '%s ganhos',
	'APS_POINTS_GLOBAL'		=> 'Geral',
	'APS_POINTS_GROWTH'		=> 'Desenvolvimento de %s',
	'APS_POINTS_LOST'		=> '%s perdidos',
	'APS_POINTS_TRADE_OFF'	=> 'Trocas de %s',
	'APS_POINTS_PER_FORUM'	=> '%s por fórum',
	'APS_POINTS_PER_GROUP'	=> '%s por grupo',

	'APS_RANDOM_USER'	=> 'Usuário aleatório',

	'APS_RECENT_ADJUSTMENTS'	=> 'Ajustes recentes',
	'APS_RECENT_ATTACHMENTS'	=> 'Anexos recentes',
	'APS_RECENT_POLL'			=> 'Enquete recente',
]);
