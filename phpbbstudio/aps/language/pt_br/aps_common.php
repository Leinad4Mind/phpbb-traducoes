<?php
/**
 *
 * phpBB Studio - Advanced Points System. An extension for the phpBB Forum Software package.
 * Brazilian Portuguese translation by eunaumtenhoid [2019][ver 1.0.4] (https://github.com/phpBBTraducoes)
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
 * Some characters you may want to copy&paste: ’ » “ ” …
 */
$lang = array_merge($lang, [
	'APS_NOTIFICATION_ADJUSTED'	=> '<strong>Seus %1$s foram ajustados</strong>',
	'APS_VIEWING_POINTS_PAGE'	=> 'Visualizando a página %s',

	'APS_POINTS_TOO_LOW'		=> 'Você não possui %s suficiente para executar esta ação.', 
	'APS_POINTS_ACTION_COST'	=> 'O custo dessa ação é %s',
]);
