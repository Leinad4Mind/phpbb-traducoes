Subject: New message from {AUTHOR_NAME}

Hi,

"{AUTHOR_NAME}" posted a new message on "{SITENAME}".

To see the message, use this link:
{U_ITEM_LINK}

To stop receiving these types of notifications, use this link:
{U_UNSUBSCRIBE}

{EMAIL_SIG}
