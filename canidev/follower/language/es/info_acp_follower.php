<?php
/**
* [Spanish [Es]]
* @package cBB Follower
* @version 2.0.3 01/06/2022
*
* @copyright (c) 2022 CaniDev
* @license https://creativecommons.org/licenses/by-nc/4.0/
*/

// DO NOT CHANGE
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

/**
 * @acp_follower
*/
$lang = array_merge($lang, array(
	'ACP_CAT_FOLLOWER'			=> 'Seguidor de usuarios',

	'ACP_FOLLOWER_COMMENTS'			=> 'Configuración de comentarios',
	'ACP_FOLLOWER_CONFIG'			=> 'Configuración principal',
	'ACP_FOLLOWER_CONFIG_EXPLAIN'	=> '¡Gracias por escoger cBB Follower como complemento para su foro!<br />Desde esta página puede modificar las principales características del seguidor de usuarios.',
	'ACP_FOLLOWER_TOOLS'			=> 'Herramientas',
	'ACP_FOLLOWER_TOOLS_EXPLAIN'	=> 'Aquí encontrará herramientas adicionales para la administración de la extensión.',

	'FWR_ADVANCED_COMMENTS'			=> 'Información avanzada en los comentarios',
	'FWR_ADVANCED_COMMENTS_EXPLAIN'	=> 'Define si se permitirán incluir imágenes en los comentarios del muro.',
	'FWR_ALLOW_COMMENTS'			=> 'Activar comentarios entre usuarios',
	'FWR_DEFAULT_PROFILE'			=> 'Establecer como perfil por defecto',
	'FWR_DEFAULT_PROFILE_EXPLAIN'	=> 'Si se habilita, el muro y movimiento de los usuarios se mostrará en lugar de su perfil convencional.',
	'FWR_MAXLENGTH'					=> 'Caracteres máximos',
	'FWR_MAXLENGTH_EXPLAIN'			=> 'Límite de caracteres permitidos en los comentarios.',
	'FWR_POPUP_ALL'					=> 'Mostrar en todo el foro',
	'FWR_POPUP_NONE'				=> 'No mostrar',
	'FWR_POPUP_PROFILE'				=> 'Mostrar solo en el perfil',
	'FWR_ROW_LIMIT'					=> 'Filas máximas',
	'FWR_ROW_LIMIT_EXPLAIN'			=> 'Máximo número de filas que se mostrarán simultáneamente.',
	'FWR_RESIZE_IMAGES'				=> 'Redimensionar imágenes',
	'FWR_RESIZE_IMAGES_EXPLAIN'		=> 'Si se habilita, las imágenes subidas serán redimensionadas al tamaño máximo especificado para los adjuntos del foro.',
	'FWR_RESYNC_COMMENTS'			=> 'Sincronizar comentarios',
	'FWR_RESYNC_COMMENTS_EXPLAIN'	=> 'Recalcula el número total de comentarios publicados por cada usuario.',
	'FWR_RESYNC_FOLLOWERS'			=> 'Sincronizar seguidores',
	'FWR_RESYNC_FOLLOWERS_EXPLAIN'	=> 'Recalcula el número total de seguidores de cada usuario.',
	'FWR_RESYNC_INTERACTIONS'			=> 'Sincronizar interacciones',
	'FWR_RESYNC_INTERACTIONS_EXPLAIN'	=> 'Recalcula el número de interacciones (Me gusta/Compartir) de los posts y comentarios.',
	'FWR_RESYNC_SUCCESS'			=> 'La sincronización se ha efectuado correctamente',
	'FWR_STORE_TIME'				=> 'Guardar comentarios de usuarios',
	'FWR_STORE_TIME_EXPLAIN'		=> 'Si se establece un tiempo límite, los comentarios que estén fuera de ese límite se borrarán automáticamente.',
	'FWR_USER_POPUP'				=> 'Mostrar cuadro flotante de información',
	'FWR_USER_POPUP_EXPLAIN'		=> 'Determina en que circunstancias se mostrará un cuadro flotante con información del usuario al pasar el ratón por encima de su nombre.',
	
	'NO_LIMIT'		=> 'Sin límite',
	'ONE_DAY'		=> 'Un día',
	'ONE_WEEK'		=> 'Una semana',
	'ONE_MONTH'		=> 'Un mes',
	'ONE_YEAR'		=> 'Un año',
	'TWO_YEARS'		=> 'Dos años',
	
	'LOG_FWR_CONFIG'				=> '<strong>cBB Follower</strong> Modificada configuración principal',
	'LOG_FWR_RESYNC_COMMENTS'		=> '<strong>cBB Follower</strong> Comentarios sincronizados',
	'LOG_FWR_RESYNC_FOLLOWERS'		=> '<strong>cBB Follower</strong> Seguidores sincronizados',
	'LOG_FWR_RESYNC_INTERACTIONS'	=> '<strong>cBB Follower</strong> Interacciones sincronizadas',
));
