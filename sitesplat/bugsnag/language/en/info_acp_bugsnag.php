<?php
/**
*
* BBcompress
*
* @copyright (c) 2015 SiteSplat All rights reserved
* @license Proprietary
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_BUGSNAG_TITLE'			  => 'Bugsnag',
	'ACP_BUGSNAG_SETTING_SAVED'	  => 'Bugsnag settings saved.',
	'ACP_BUGSNAG_API_KEY'		  => 'Bugsnag API Key',
	'ACP_BUGSNAG_API_KEY_EXPLAIN' => '<br />-Create the API key on: <a href="https://bugsnag.com/" target="_blank">https://bugsnag.com/</a> (free account available)<br /> -select PHP under create a new project on bugsnag <br /> -Then hit settings button and look for the API key <br />-Paste the API key in the filed here and hit submit to save it.<br /> You will receive emails notification when error occurs.',
	'ACP_BUGSNAG_SETTINGS'		  => 'Settings',
	'SS_HELPER_NOTY'	          => 'SiteSplat BBcore does not exist!<br />Download the <a href="http://sitesplat.com" target="_blank">BBcore</a> and copy the BBcore folder into your sitesplat extension folder.',
	'BUGSNAG_NOTICE'	          => '<div class="phpinfo"><p><br />-Create the API key on: <a href="https://bugsnag.com/" target="_blank">https://bugsnag.com/</a> (free account available)<br /> -select PHP under create a new project on bugsnag <br /> -Then hit settings button and look for the API key <br /> -Once done, under the extensions tab, Bugsnag settings enter the API key to start monitoring the baord.</p></div>',
));
